package patterns;

import patterns.factorymethod.Creator;
import patterns.factorymethod.CreatorCar;
import patterns.factorymethod.CreatorTrain;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Creator> creators = new ArrayList<Creator>();
        creators.add(CreatorCar.CreatorCarHolder.getInstance());
        creators.add(CreatorTrain.CreatorTrainHolder.getInstance());

        for(Creator creator : creators) {
            System.out.println(creator.create());
        }
    }
}
