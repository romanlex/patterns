package patterns.adapter;

public class AdapterCompositionVk implements Bot {
    private VkBot bot = new VkBot();

    private int searchGroupId() {
        return 1;
    }

    @Override
    public void sendMessage(String message, int userId) {
        bot.sendMessage(message, true, userId);
    }

    @Override
    public void sendSpam(String message, int ppl) {
        bot.sendSpam(ppl, message, 500);
    }

    @Override
    public void sleep(float millis) {

    }
}
