package patterns.adapter;

public class AdapterGeneralizeTelegram extends TelegramBot implements Bot {

    private int searchGroupId() {
        return 1;
    }

    @Override
    public void sendMessage(String message, int userId) {
        this.sendMessage(message, searchGroupId(), userId);
    }

    @Override
    public void sendSpam(String message, int ppl) {
        this.sendSpam(ppl, message);
    }

    @Override
    public void sleep(float millis) {
        this.sleep((int)millis);
    }
}
