package patterns.adapter;

public interface Bot {
    void sendMessage(String message, int userId);
    void sendSpam(String message, int ppl);
    void sleep(float millis);
}
