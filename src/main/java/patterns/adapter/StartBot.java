package patterns.adapter;

public class StartBot {
    public static void main(String[] args) {
        Bot bot1 = new AdapterGeneralizeTelegram();
        Bot bot2 = new AdapterCompositionVk();

        bot1.sendMessage("Message sent to Telegram", 1);
        bot1.sendSpam("Spam sent to Telegram", 5);
        bot1.sleep(1000);

        bot2.sendMessage("Message sent to VK", 2);
        bot2.sendSpam("Spam sent to VK", 8);
        bot2.sleep(1000);

    }
}
