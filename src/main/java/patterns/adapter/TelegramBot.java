package patterns.adapter;

public class TelegramBot {

    public void sendMessage(String message, int groupId, int userId) {
        System.out.println("Sent message to group and user");
    }

    public void sendSpam(int ppl, String message) {
        System.out.println("Sent spam to peoples");
    }

    public void sleep(int millis) {
        System.out.println("Sleep to millis");
    }

}
