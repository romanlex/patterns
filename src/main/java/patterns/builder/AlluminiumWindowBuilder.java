package patterns.builder;

abstract class AlluminiumWindowBuilder {
    protected AlluminiumWindow alluminiumWindow;

    public void create() {
        alluminiumWindow = new AlluminiumWindow();
    }

    abstract void buildModel();
    abstract void buildHeight();
    abstract void buildWidth();

    public AlluminiumWindow getAlluminiumWindow() {
        if(alluminiumWindow != null)
            return alluminiumWindow;
        else return new AlluminiumWindow();
    }
}
