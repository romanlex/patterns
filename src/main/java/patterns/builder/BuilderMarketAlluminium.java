package patterns.builder;

public class BuilderMarketAlluminium extends AlluminiumWindowBuilder {
    void buildModel() {
        alluminiumWindow.setModel("asdasd");
    }

    void buildHeight() {
        alluminiumWindow.setHeight(1000);
    }

    void buildWidth() {
        alluminiumWindow.setWidth(1000);
    }
}
