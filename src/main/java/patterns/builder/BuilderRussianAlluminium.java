package patterns.builder;

public class BuilderRussianAlluminium extends AlluminiumWindowBuilder {
    void buildModel() {
        alluminiumWindow.setModel("asdasd2");
    }

    void buildHeight() {
        alluminiumWindow.setHeight(3000);
    }

    void buildWidth() {
        alluminiumWindow.setWidth(3000);
    }
}
