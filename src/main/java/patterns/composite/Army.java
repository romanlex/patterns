package patterns.composite;

import java.util.List;

public class Army implements IComponent {

    private Integer soldats;
    private List<IComponent> okrugs;

    public Army(List<IComponent> okrugs) {
        this.okrugs = okrugs;
    }

    @Override
    public int getSoldats() {
        int price = 0;
        for (IComponent okrug: okrugs) {
            price += okrug.getSoldats();
        }
        return price;
    }

    @Override
    public int getBoepripasi() {
        int price = 0;
        for (IComponent okrug: okrugs) {
            price += okrug.getBoepripasi();
        }
        return price;
    }

    @Override
    public int getBudget() {
        int price = 0;
        for (IComponent okrug: okrugs) {
            price += okrug.getBudget();
        }
        return price;
    }

    @Override
    public void add(IComponent component) {
        okrugs.add(component);
    }

    @Override
    public void sub(IComponent component) {
        okrugs.add(component);
    }

}
