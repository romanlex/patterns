package patterns.composite;

import java.util.List;

public class Batalion implements IComponent {
    private Integer soldats;
    private List<IComponent> rots;

    public Batalion(List<IComponent> rots) {
        this.rots = rots;
    }

    @Override
    public int getSoldats() {
        int price = 0;
        for (IComponent rota: rots) {
            price += rota.getSoldats();
        }
        return price;
    }

    @Override
    public int getBoepripasi() {
        int price = 0;
        for (IComponent rota: rots) {
            price += rota.getBoepripasi();
        }
        return price;
    }

    @Override
    public int getBudget() {
        int price = 0;
        for (IComponent rota: rots) {
            price += rota.getBudget();
        }
        return price;
    }
    @Override
    public void add(IComponent component) {
        rots.add(component);
    }

    @Override
    public void sub(IComponent component) {
        rots.add(component);
    }

}
