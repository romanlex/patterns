package patterns.composite;

import java.util.List;

public class Chast implements IComponent {
    private Integer soldats;
    private List<IComponent> batalions;

    public Chast(List<IComponent> batalions) {
        this.batalions = batalions;
    }

    @Override
    public int getSoldats() {
        int price = 0;
        for (IComponent batalion: batalions) {
            price += batalion.getSoldats();
        }
        return price;
    }

    @Override
    public int getBoepripasi() {
        int price = 0;
        for (IComponent batalion: batalions) {
            price += batalion.getBoepripasi();
        }
        return price;
    }

    @Override
    public int getBudget() {
        int price = 0;
        for (IComponent batalion: batalions) {
            price += batalion.getBudget();
        }
        return price;
    }

    @Override
    public void add(IComponent component) {
        batalions.add(component);
    }

    @Override
    public void sub(IComponent component) {
        batalions.add(component);
    }

}
