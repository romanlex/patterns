package patterns.composite;

public class Department implements IComponent {
    private Integer soldats;
    private Integer boepripases;
    private Integer budget;

    public Department(Integer soldats, Integer boepripases, Integer budget) {
        this.soldats = soldats;
        this.boepripases = boepripases;
        this.budget = budget;
    }

    @Override
    public int getSoldats() {
        return soldats;
    }

    @Override
    public int getBoepripasi() {
        return boepripases;
    }

    @Override
    public int getBudget() {
        return budget;
    }

    @Override
    public void add(IComponent component) {

    }

    @Override
    public void sub(IComponent component) {

    }

}
