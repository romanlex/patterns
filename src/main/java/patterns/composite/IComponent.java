package patterns.composite;

public interface IComponent {
    int getSoldats();
    int getBoepripasi();
    int getBudget();
    void add(IComponent component);
    void sub(IComponent component);
}
