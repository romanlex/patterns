package patterns.composite;

import java.util.List;

public class Okrug implements IComponent {
    private Integer soldats;
    private List<IComponent> chasts;

    public Okrug(List<IComponent> chasts) {
        this.chasts = chasts;
    }

    @Override
    public int getSoldats() {
        int price = 0;
        for (IComponent chast: chasts) {
            price += chast.getSoldats();
        }
        return price;
    }

    @Override
    public int getBoepripasi() {
        int price = 0;
        for (IComponent chast: chasts) {
            price += chast.getBoepripasi();
        }
        return price;
    }

    @Override
    public int getBudget() {
        int price = 0;
        for (IComponent chast: chasts) {
            price += chast.getBudget();
        }
        return price;
    }

    @Override
    public void add(IComponent component) {
        chasts.add(component);
    }

    @Override
    public void sub(IComponent component) {
        chasts.add(component);
    }

}
