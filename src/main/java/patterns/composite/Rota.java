package patterns.composite;

import java.util.List;

public class Rota implements IComponent {
    private Integer soldats;
    private List<IComponent> vzvods;

    public Rota(List<IComponent> vzvods) {
        this.vzvods = vzvods;
    }

    @Override
    public int getSoldats() {
        int price = 0;
        for (IComponent vzvod: vzvods) {
            price += vzvod.getSoldats();
        }
        return price;
    }

    @Override
    public int getBoepripasi() {
        int price = 0;
        for (IComponent vzvod: vzvods) {
            price += vzvod.getBoepripasi();
        }
        return price;
    }

    @Override
    public int getBudget() {
        int price = 0;
        for (IComponent vzvod: vzvods) {
            price += vzvod.getBudget();
        }
        return price;
    }

    @Override
    public void add(IComponent component) {
        vzvods.add(component);
    }

    @Override
    public void sub(IComponent component) {
        vzvods.add(component);
    }

}
