package patterns.composite;

import java.util.ArrayList;
import java.util.List;

public class StartComposite {
    public static void main(String[] args) {
        List<IComponent> okrugs = new ArrayList<>();
        List<IComponent> chasts = new ArrayList<>();
        List<IComponent> batalions = new ArrayList<>();
        List<IComponent> rots = new ArrayList<>();
        List<IComponent> vzvods = new ArrayList<>();

        IComponent department = new Department(100,100,1000);
        IComponent department2 = new Department(50,200,800);
        vzvods.add(department);
        vzvods.add(department2);

        IComponent rota = new Rota(vzvods);
        rots.add(rota);

        IComponent batalion = new Batalion(rots);
        batalions.add(batalion);

        IComponent chast = new Chast(batalions);
        chasts.add(chast);

        IComponent okrug = new Okrug(chasts);
        okrugs.add(okrug);

        IComponent army = new Army(okrugs);

        System.out.println("Boepripasi: " + army.getBoepripasi());
        System.out.println("Soldats: " + army.getSoldats());
        System.out.println("Budget: " + army.getBudget());

    }
}
