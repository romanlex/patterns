package patterns.composite;

import java.util.List;

public class Vzvod implements IComponent {
    private Integer soldats;
    private List<IComponent> departments;

    public Vzvod(List<IComponent> departments) {
        this.departments = departments;
        for (IComponent department: departments) {
            this.departments.add(department);
        }
    }

    @Override
    public int getSoldats() {
        int price = 0;
        for (IComponent department: departments) {
            price += department.getSoldats();
        }
        return price;
    }

    @Override
    public int getBoepripasi() {
        int price = 0;
        for (IComponent department: departments) {
            price += department.getBoepripasi();
        }
        return price;
    }

    @Override
    public int getBudget() {
        int price = 0;
        for (IComponent department: departments) {
            price += department.getBudget();
        }
        return price;
    }

    @Override
    public void add(IComponent component) {
        departments.add(component);
    }

    @Override
    public void sub(IComponent component) {
        departments.add(component);
    }
}
