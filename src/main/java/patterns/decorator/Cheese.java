package patterns.decorator;

public class Cheese extends PizzaComponent {

    public Cheese(ComponentInterface component) {
        super(component);
    }

    @Override
    public void showComponent() {
        super.showComponent();
        System.out.println("Cheese");
    }
}
