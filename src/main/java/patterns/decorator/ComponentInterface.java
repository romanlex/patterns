package patterns.decorator;

public interface ComponentInterface {
    void showComponent();
}
