package patterns.decorator;

public class MakePizza {

    public static void main(String[] args) {
        Pizza pizza = new Pizza();
        Cheese cheese = new Cheese(new Tomato(new Cheese(pizza)));
        cheese.showComponent();
    }
}
