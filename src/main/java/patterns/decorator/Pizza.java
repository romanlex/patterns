package patterns.decorator;

public class Pizza implements ComponentInterface {

    @Override
    public void showComponent() {
        System.out.println("Pizza");
    }
}
