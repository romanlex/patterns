package patterns.facade;

public class Brain {
    void think(String thought) {
        System.out.println("I think of " + thought);
    }

    void stayCold(int temp) {
        System.out.println("Brain temperature is" + temp);
    }
}
