package patterns.facade;

public class Heart {
    void takeRightm() {
        System.out.println("tuk-tuk");
    }

    void stayHot(int temp) {
        System.out.println("out temperature " + temp);
    }
}
