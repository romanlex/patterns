package patterns.facade;

public class Human {
    private Brain brain;
    private Hands hands;
    private Heart heart;

    public Human() {
        brain = new Brain();
        hands = new Hands();
        heart = new Heart();
    }

    public void live() {
        brain.stayCold(100);
        brain.think("8 marta");
        hands.doWork();
        heart.stayHot(36);
        heart.takeRightm();
    }
}
