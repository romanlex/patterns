package patterns.factorymethod;

public abstract class Creator {
    public abstract Transport create();
}


