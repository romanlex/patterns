package patterns.factorymethod;

public class CreatorCar extends Creator {

    private CreatorCar(){};

    @Override
    public Transport create() {
        return new Car();
    }

    public static class CreatorCarHolder {
        public static final CreatorCar INSTANCE = new CreatorCar();

        public static CreatorCar getInstance() {
            return INSTANCE;
        }
    }
}


