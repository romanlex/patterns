package patterns.factorymethod;

public class CreatorTrain extends Creator {

    @Override
    public Transport create() {
        return new Train();
    }


    public static class CreatorTrainHolder {
        public static final CreatorTrain INSTANCE = new CreatorTrain();

        public static CreatorTrain getInstance() {
            return INSTANCE;
        }
    }

}
