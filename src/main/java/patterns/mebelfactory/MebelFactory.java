package patterns.mebelfactory;

abstract public class MebelFactory {
    abstract public Chair createChair();
    abstract public Table createTable();
    abstract public Sofa createSofa();
    abstract public Taburetka taburetkaCreator();
}


