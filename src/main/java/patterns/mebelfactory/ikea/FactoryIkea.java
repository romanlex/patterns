package patterns.mebelfactory.ikea;

import patterns.mebelfactory.*;

public class FactoryIkea extends MebelFactory {
    public Chair createChair() {
        return new ChairIkea();
    }

    public Table createTable() {
        return new TableIkea();
    }

    public Sofa createSofa() {
        return new SofaIkea();
    }

    public Taburetka taburetkaCreator() {
        return new TaburetkaIkea();
    }
}
