package patterns.mebelfactory.mnogomebeli;

import patterns.mebelfactory.*;

public class FactoryMnogoMebeli extends MebelFactory {
    public Chair createChair() {
        return new ChairMnogomebeli();
    }

    public Table createTable() {
        return new TableMnogomebeli();
    }

    public Sofa createSofa() {
        return new SofaMnogomebeli();
    }

    public Taburetka taburetkaCreator() {
        return new TaburetkaMnogomebeli();
    }

}
