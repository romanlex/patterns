package patterns.most;

public interface IHospital {
    void heal();
}
