package patterns.most;

public abstract class Patient {
    protected IHospital iHospital;

    protected Patient(IHospital iHospital) {
        this.iHospital = iHospital;
    }

    public abstract void heal();
}
