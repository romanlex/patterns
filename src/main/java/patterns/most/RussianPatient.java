package patterns.most;

public class RussianPatient extends Patient {
    protected RussianPatient(IHospital iHospital) {
        super(iHospital);
    }

    @Override
    public void heal() {
        iHospital.heal();
    }
}
