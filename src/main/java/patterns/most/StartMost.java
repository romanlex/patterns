package patterns.most;

public class StartMost {
    public static void main(String[] args) {
        Patient kyleRease = new RussianPatient(new Surgery());
        Patient saraConnor = new RussianPatient(new Ginekology());
        Patient[] patients = {kyleRease, saraConnor};

        for (Patient patient:patients) {
            patient.heal();
        }
    }
}
