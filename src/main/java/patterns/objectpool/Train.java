package patterns.objectpool;

public class Train {
    public static void main(String[] args) throws InterruptedException {
        TrainCargoPool pool = new TrainCargoPool();
        Cargo cargo2 = pool.checkOut(); // 7852e922
        System.out.println(cargo2);
        Cargo cargo = pool.checkOut(); // 4e25154f
        System.out.println(cargo);
        pool.checkIn(cargo);

        cargo = pool.checkOut(); // 4e25154f
        System.out.println(cargo);

        pool.checkIn(cargo);
        System.out.println(pool.getUnlocked());
        System.out.println(pool.getLocked());

        Thread.sleep(4000);

        cargo = pool.checkOut(); //70dea4e
        System.out.println(cargo);

        System.out.println(pool.getUnlocked());
    }
}

