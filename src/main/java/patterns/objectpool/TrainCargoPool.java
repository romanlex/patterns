package patterns.objectpool;

public class TrainCargoPool extends ObjectPool<Cargo> {
    public TrainCargoPool() {
        super();
    }

    @Override
    protected Cargo create() {
        return new Cargo();
    }

    @Override
    public boolean validate(Cargo o) {
        return true;
    }

    @Override
    public void expire(Cargo o) {
        System.out.println("expire");
    }
}
